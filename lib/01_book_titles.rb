class Book
  attr_accessor :title
  # TODO: your code goes here!

  def title
    non_capital_words = ["the", "a", "an", "and", "in", "the", "of"]
    words = @title.split
    new_arr = []
    words.each_with_index do |word, idx|
      if idx == 0
        new_arr << word.capitalize
      elsif non_capital_words.include?(word)
        new_arr << word
      else
        new_arr << word.capitalize
      end
    end
    new_arr.join(" ")

  end


end
