class Timer
  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    int = @seconds
    time_seconds = (int % 60).to_s
    time_minutes = (int % 3600 / 60).to_s
    time_hours = (int / 3600).to_s
    time_arr = [time_hours, time_minutes, time_seconds]
    final_arr = []
    time_arr.each do |x|
      if x.length == 1
        final_arr << ("0" + x)
      else
        final_arr << x
      end
    end
    final_arr.join(":")


  end





end
