class Dictionary
  attr_accessor :entries
  attr_reader :keywords

  # TODO: your code goes here!

  def initialize
    @entries = {}
  end

  def add(options)
    if options.class == String
      @entries[options] = nil
    else
      key = options.keys.join
      value = options.values.join
      @entries[key] = value
    end
    @keywords = @entries.keys.sort
  end

  def include?(arg)
    self.entries.include?(arg)
  end

  def find(arg)
    keys_array = self.entries.keys
    valid_keys = keys_array.select { |word| word.include?(arg) }
    if valid_keys.length == 0
      return {}
    else
      self.entries.select { |entry, | valid_keys.include?(entry)}
    end
  end

  def printable
    new_arr = []
    self.entries.sort.each { |k, v| new_arr << %Q([#{k}] "#{v}")}
    new_arr.join("\n")
  end

end
