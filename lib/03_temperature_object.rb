class Temperature
  attr_reader :options
  # TODO: your code goes here!
  def initialize(options = {})
    default = { f: nil, c: nil }
    @options = default.merge(options)
    @celsius = @options[:c]
    @fahrenheit = @options[:f]

  end

  def fahrenheit=(temp)
    @fahrenheit = temp
  end

  def celsius=(temp)
    @celsius = temp
  end

  def self.ctof(int)
    int * 9.0 / 5 + 32
  end

  def self.ftoc(int)
    (int - 32) * 5 / 9
  end

  def in_fahrenheit
    if @celsius.nil?
      @fahrenheit
    else
      self.class.ctof(@celsius)
    end
  end

  def in_celsius
    if @fahrenheit.nil?
      @celsius
    else
      self.class.ftoc(@fahrenheit)
    end
  end

  def self.from_celsius(int)
    return Temperature.new(c: int)
  end

  def self.from_fahrenheit(int)
    return Temperature.new(f: int)
  end
end

  class Celsius < Temperature
    def initialize(int)
      self.celsius = int
    end

  end

  class Fahrenheit < Temperature
    def initialize(int)
      self.fahrenheit = int
    end

  end
